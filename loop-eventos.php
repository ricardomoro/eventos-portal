<?php while (have_posts()) : the_post(); ?>
    <article class="col-xs-6 col-sm-4 col-md-3">
        <a href="<?php echo get_post_meta($post->ID, 'link', true); ?>" class="thumbnail" data-toggle="tooltip" data-placement="bottom" title="<?php the_title(); ?>">
        <?php if (has_post_thumbnail()): ?>
            <?php the_post_thumbnail(null, array('alt' => 'Ir para o evento: ' . get_the_title())); ?>
        <?php else: ?>
            <?php the_title(); ?>
        <?php endif; ?>
        </a>
    </article>
<?php endwhile; ?>
