<?php get_header(); ?>

<?php breadcrumb(); ?>

<section class="container" id="conteudo">
    <div class="row">
        <?php get_template_part('loop', 'eventos'); ?>
    </div>
</section>

<?php get_footer(); ?>
