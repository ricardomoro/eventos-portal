<?php get_header(); ?>

<?php breadcrumb(); ?>

<section class="container" id="conteudo">
    <div class="row">
    <?php
        // Eventos ativos
        $args = array(
            'tax_query' => array(
                array(
                    'taxonomy' => 'category',
                    'field'    => 'slug',
                    'terms'    => 'inativos',
                    'operator' => 'NOT IN',
                ),
            ),
        );
        query_posts($args);

        if (have_posts()):
            get_template_part('loop', 'eventos');
        else:
    ?>
        <div class="col-xs-12">
            <div class="alert alert-warning" role="alert">
                <p><strong>Aten&ccedil;&atilde;o!</strong> N&atilde;o existem eventos em andamento no momento.</p>
            </div>
        </div>
    <?php endif; ?>

    <?php
        // Eventos inativos
        $args = array(
            'tax_query' => array(
                array(
                    'taxonomy' => 'category',
                    'field'    => 'slug',
                    'terms'    => 'inativos',
                ),
            ),
        );
        query_posts($args);

        if (have_posts()):
            get_template_part('loop', 'eventos');
        else:
    ?>
        <div class="col-xs-12">
            <div class="alert alert-warning" role="alert">
                <p><strong>Aten&ccedil;&atilde;o!</strong> N&atilde;o existem eventos conclu&iacute;dos.</p>
            </div>
        </div>
    <?php endif; ?>
    </div>
</section>

<?php get_footer(); ?>
