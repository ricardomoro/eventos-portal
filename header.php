<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="index,follow">
    <meta name="author" content="Diretoria de Comunicação do IFRS">

    <!-- Título -->
    <?php echo get_template_part('partials/title'); ?>

    <!-- Favicon -->
    <?php echo get_template_part('partials/favicons'); ?>

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <!-- Barra do Governo -->
    <?php echo get_template_part('partials/barrabrasil'); ?>

    <!-- Barra de Acessibilidade -->
    <div class="container">
        <div class="row" id="atalhos">
            <div class="col-xs-6 col-md-2">
                <?php get_template_part('partials/menu'); ?>
            </div>
            <div class="col-sm-6 col-md-10 hidden-xs">
                <ul class="barra-atalhos pull-right">
                    <li><a href="#conteudo"                 accesskey="1"><span class="sr-only">Ir para o</span> Conteúdo <span class="badge">1</span></a></li>
                    <li><a href="#menu-principal"           accesskey="2"><span class="sr-only">Ir para o</span> Menu <span class="badge">2</span></a></li>
                    <li><a href="#search-acessibilidade"    accesskey="3"><span class="sr-only">Ir para a</span> Pesquisa <span class="badge">3</span></a></li>
                    <li><a href="#contraste-switch"         accesskey="4" id="contraste-switch"><span class="glyphicon glyphicon-adjust"></span> <span class="sr-only">Alterar </span>Contraste <span class="badge">4</span></a></li>
                </ul>
            </div>
        </div>
    </div>

     <header>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
					<?php if ( get_header_image() ) : ?>
                        <h1>
                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                                <img src="<?php header_image(); ?>" class="img-responsive" alt="<?php bloginfo('name'); ?>">
                            </a>
                        </h1>
                    <?php else : ?>
                        <h1 class="sr-only"><?php bloginfo('name'); ?></h1>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </header>

    <a href="#inicio-conteudo" id="inicio-conteudo" class="sr-only">In&iacute;cio do Conte&uacute;do</a>
