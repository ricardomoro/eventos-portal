<?php
//Habilita imagens destaques
add_theme_support( 'post-thumbnails' );

//Registra o menu principal
register_nav_menus(
    array(
        'menu-principal' => 'Menu Principal',
    )
);

// assets
require_once('inc/assets.php');

// Post Meta Box
require_once('inc/url_box.php');
