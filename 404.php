<?php get_header(); ?>

<div class="container" id="conteudo">
    <div class="row">
        <div class="col-md-12">
           <section class="conteudo">
                <h2>Esta página não existe. Erro 404.</h2>
                <p>Parece que nada foi encontrado neste local. Tente pesquisar.</p>

                <?php get_search_form(); ?>
            </section>
        </div>
    </div>
</div>

<?php get_footer(); ?>
