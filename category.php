<?php get_header(); ?>

<?php breadcrumb(); ?>

<section class="container" id="conteudo">
    <div class="row">
        <div class="col-xs-12">
            <h2><?php single_cat_title( '', true ); ?></h2>
        </div>
    </div>
    <div class="row">
    <?php if (have_posts()) : ?>
        <?php get_template_part('loop', 'eventos'); ?>
    <?php else : ?>
        <div class="col-xs-12">
            <div class="alert alert-warning" role="alert">
                <p><strong>Aten&ccedil;&atilde;o!</strong> N&atilde;o existem eventos nessa categoria.</p>
            </div>
        </div>
    <?php endif; ?>
    </div>
</section>

<script>
    $(document).ready(function() {
        $('.thumbnail').tooltip();
    });
</script>

<?php get_footer(); ?>
