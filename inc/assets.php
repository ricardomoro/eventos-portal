<?php
function load_styles_eventos_portal() {
    /* wp_enqueue_style( $handle, $src, $deps, $ver, $media ); */

    if (WP_DEBUG) {
        wp_enqueue_style('css-eventos-portal', get_stylesheet_directory_uri().'/css/eventos-portal.css', array(), false, 'all');
    } else {
        wp_enqueue_style('css-eventos-portal', get_stylesheet_directory_uri().'/css/eventos-portal.css', array(), false, 'all');
    }
}

function load_scripts_eventos_portal() {
    if ( ! is_admin() ) {
        wp_deregister_script('jquery');
    }

    /* wp_register_script( $handle, $src, $deps, $ver, $in_footer ); */

    if (WP_DEBUG) {
        wp_enqueue_script( 'html5shiv', get_stylesheet_directory_uri().'/vendor/html5shiv/dist/html5shiv.js', array(), false, false );
        wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 9' );
        wp_enqueue_script( 'html5shiv-print', get_stylesheet_directory_uri().'/vendor/html5shiv/dist/html5shiv-printshiv.js', array(), false, false );
        wp_script_add_data( 'html5shiv-print', 'conditional', 'lt IE 9' );

        wp_enqueue_script( 'respond', get_stylesheet_directory_uri().'/vendor/respond-minmax/dest/respond.src.js', array(), false, false );
        wp_script_add_data( 'respond', 'conditional', 'lt IE 9' );
        wp_enqueue_script( 'respond-matchmedia', get_stylesheet_directory_uri().'/vendor/respond-minmax/dest/respond.matchmedia.addListener.src.js', array(), false, false );
        wp_script_add_data( 'respond-matchmedia', 'conditional', 'lt IE 9' );

        wp_enqueue_script( 'jquery', get_stylesheet_directory_uri().'/vendor/jquery/dist/jquery.js', array(), false, false );
        wp_enqueue_script( 'bootstrap', get_stylesheet_directory_uri().'/vendor/bootstrap-sass/assets/javascripts/bootstrap.js', array('jquery'), false, false );
        wp_enqueue_script( 'bootstrap-accessibility', get_stylesheet_directory_uri().'/vendor/bootstrapaccessibilityplugin/plugins/js/bootstrap-accessibility.js', array('bootstrap'), false, false );

        wp_enqueue_script( 'tooltip-config', get_stylesheet_directory_uri().'/src/tooltip-config.js', array('jquery'), false, true );
    } else {
        wp_enqueue_script( 'html5shiv', get_stylesheet_directory_uri().'/vendor/html5shiv/dist/html5shiv.min.js', array(), false, false );
        wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 9' );
        wp_enqueue_script( 'html5shiv-print', get_stylesheet_directory_uri().'/vendor/html5shiv/dist/html5shiv-printshiv.min.js', array(), false, false );
        wp_script_add_data( 'html5shiv-print', 'conditional', 'lt IE 9' );

        wp_enqueue_script( 'respond', get_stylesheet_directory_uri().'/vendor/respond-minmax/dest/respond.min.js', array(), false, false );
        wp_script_add_data( 'respond', 'conditional', 'lt IE 9' );
        wp_enqueue_script( 'respond-matchmedia', get_stylesheet_directory_uri().'/vendor/respond-minmax/dest/respond.matchmedia.addListener.min.js', array(), false, false );
        wp_script_add_data( 'respond-matchmedia', 'conditional', 'lt IE 9' );

        wp_enqueue_script( 'jquery', get_stylesheet_directory_uri().'/vendor/jquery/dist/jquery.min.js', array(), false, false );
        wp_enqueue_script( 'bootstrap', get_stylesheet_directory_uri().'/vendor/bootstrap-sass/assets/javascripts/bootstrap.min.js', array('jquery'), false, false );
        wp_enqueue_script( 'bootstrap-accessibility', get_stylesheet_directory_uri().'/vendor/bootstrapaccessibilityplugin/plugins/js/bootstrap-accessibility.min.js', array('bootstrap'), false, false );

        wp_enqueue_script( 'tooltip-config', get_stylesheet_directory_uri().'/js/tooltip-config.min.js', array('jquery'), false, true );

        wp_enqueue_script( 'js-barra-brasil', '//barra.brasil.gov.br/barra.js', array(), false, true );
    }
}

add_action( 'wp_enqueue_scripts', 'load_styles_eventos_portal', 1 );
add_action( 'wp_enqueue_scripts', 'load_scripts_eventos_portal', 1 );
