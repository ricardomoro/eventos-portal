<?php
add_filter( 'rwmb_meta_boxes', 'post_meta_boxes' );
function post_meta_boxes( $meta_boxes ) {
    $blog_list = wp_get_sites();
    $urls = array();
    foreach ($blog_list as $blog) {
        $url = 'http://' . $blog['domain'] . $blog['path'];
        $urls[] = $url;
    }
    $meta_boxes[] = array(
        'title'      => __( 'Evento', 'eventos' ),
        'post_types' => 'post',
        'fields'     => array(
            array(
                'id'   => 'link',
                'name' => __( 'Endereço', 'eventos' ),
                'type' => 'text',
                'datalist' => array(
                    'options' => $urls,
                ),
            ),
        ),
    );
    return $meta_boxes;
}
